var mymodule = require('./mymodule.js');
var dir = process.argv[2];
var ext = process.argv[3];

mymodule(dir, ext, function callback (err, data) {
  if (err) 
    return console.error('There was an error:', err);

  data.forEach(function print (file) {
    console.log(file);
  });
});

