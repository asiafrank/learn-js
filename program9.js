var net = require('net');
var strftime = require('strftime');
var port = Number(process.argv[2]);

var currDate = function () {
  var date = new Date();
  return strftime('%F %H:%M', date);
}

var server = net.createServer(function (socket) {
  var date = currDate();
  socket.write(date);
  socket.end('\n');
});

server.listen(port);
