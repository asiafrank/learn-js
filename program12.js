var http = require('http');
var url = require('url');

var port = Number(process.argv[2]);

var server = http.createServer(function (req, res) {
  var urlObj = url.parse(req.url, true); 
  var date = new Date(urlObj.query['iso']);
  var result = {};

  if (urlObj.pathname === '/api/parsetime') {
    result.hour = date.getHours();
    result.minute = date.getMinutes();
    result.second = date.getSeconds();
  } else if (urlObj.pathname === '/api/unixtime') {
    result.unixtime = date.getTime();
  }

  res.writeHead(200, {'Content-Type': 'application/json'});
  res.end(JSON.stringify(result));
});

server.listen(port);
