var http = require('http');
var bl = require('bl');
var urls = process.argv.slice(2);
var i = 0;

(function jugglingAsync () {
  http.get(urls[i], function callback (response) {
    response.setEncoding('utf8');
    response.pipe(bl(function (err, data) {
      if (err)
        return console.error(err);

      console.log(data.toString());

      i++;
      if (i == urls.length) {
        return;
      } else {
        jugglingAsync();
      }
    }));
  });
})();
