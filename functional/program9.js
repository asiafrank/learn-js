var slice = Array.prototype.slice;

function logger(namespace) {
  var n = namespace;
  return function() {
    var arr = slice.call(arguments);
    console.log.apply(console, [n].concat(arr));
  }
}

module.exports = logger;
