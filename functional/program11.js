module.exports = function arrayMap(arr, fn) {
  var f = function (pre, cur, index, array) {
    pre[index] = fn(array[index]);
    return pre;
  };
  return arr.reduce(f, []);
}
