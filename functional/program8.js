function duckCount () {
  var count = 0;
  var arr = Array.prototype.slice.call(arguments);
  arr.map(function (o) {
    if (Object.prototype.hasOwnProperty.call(o, 'quack')) {
      count++;
    }
  });
  return count;
}

module.exports = duckCount;
