module.exports = function(namespace) {
  var unBoundSlice = Array.prototype.slice;
  var slice = Function.prototype.apply.bind(unBoundSlice);
  return function () {
    var arr = slice(arguments);
    console.log.apply(console, [namespace].concat(arr));
  }
}
