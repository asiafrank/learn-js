function reduce (array, callback, initValue) {
  var index = 0;
  return (function reduceX (index, initValue) {
    initValue = callback(initValue, array[index], index, array);
    if (index === array.length - 1) {
      return initValue;
    } else {
      index++;
      return reduceX (index, initValue);
    } 
  })(0, initValue);
}

module.exports = reduce
