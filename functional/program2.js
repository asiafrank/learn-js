function repeat (operation, num) {
  if (num) {
    operation();
    return repeat(operation, --num);
  }
  return;
}

module.exports = repeat
