var fs = require('fs');
var path = require('path');

function lsFilter (dir, ext, callback) {
  var arr = [];

  fs.readdir(dir, function bar (err, list) {
    if (err) return callback(err);

    list.forEach(function (file) {
      if (path.extname(file) === '.' + ext) {
        arr.push(file);
      }
    });

    callback(null, arr);
  });
}

module.exports = lsFilter;
